#!/usr/bin/zsh

function rand_delay() {
	local mult="100.0"

	sleep "$[ ($RANDOM % (${MAX_DELAY:-5} * $mult) ) / $mult + ${MIN_DELAY:-1} ]s"
}

rand_delay

[ $# -ge 1 ] && exec $@
