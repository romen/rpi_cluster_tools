#!/bin/bash

function DD() {
	dd conv=fsync bs=4096 status=progress $@
}


function unmount_partitions() {
	local dev=$1
	if mount|grep -e "^$dev" >/dev/null 2>/dev/null; then
		for d in $(mount|grep -e "^$dev" | cut -d" " -f1); do
			umount $d
		done
	fi
}

function rewrite_partition_table() {
	local dev=$1

	DD if=/dev/zero of=${dev} count=18k
	sync

	sfdisk $dev <<-EOPT
		label: dos
		label-id: 0xcfe46d59
		device: ${dev}
		unit: sectors

		${dev}1 : start=        8192, size=      129024, type=c
		${dev}2 : start=      137216, size=    31285248, type=83
	EOPT

	sync
	partprobe
}

function format_partitions() {
	local dev=$1

	local p1="/dev/$(lsblk -r -o NAME "${dev}" | sed -n 3p)"
	local p2="/dev/$(lsblk -r -o NAME "${dev}" | sed -n 4p)"


	echo "Erasing $p1"
	DD if=/dev/zero of=${p1} count=16128
	sync
	mkfs.vfat -nRPI_NOBOOT  ${p1}
	sync

	sleep 1
	partprobe

	local mntpnt=/dev/shm/mnt
	mkdir -p $mntpnt
	mount $p1 $mntpnt
	touch $mntpnt/INTENTIONALLY_EMPTY_FOR_PXE_BOOT
	sync
	umount $mntpnt

	echo "Erasing $p2"
	DD if=/dev/zero of=${p2} count=1000
	sync
	mkfs.ext4 -L"local_storage" -b 4096 -O "^huge_file" ${p2}
	sync

	sleep 1
	partprobe

	mkdir -p $mntpnt
	mount $p2 $mntpnt
	mkdir -p $mntpnt/var/{log,tmp}
	mkdir -p $mntpnt/var/.overlayfs/workdirs/{log,tmp}
	chmod 777 $mntpnt/var/tmp
	sync
	umount $mntpnt

	sleep 1
	partprobe

}

function confirm() {
    # call with a prompt string or use a default
    read -r -p "${1:-Are you sure? [y/N]} " response
    case "$response" in
        [yY][eE][sS]|[yY])
            true
            ;;
        *)
            false
            ;;
    esac
}

if [[ "$1" == "-f" ]]; then
	FORCE=1
	shift
fi

function confirm_dev() {
	fdisk -l $1

	if [[ "$FORCE" == "1" ]]; then
		true
	else
		confirm || exit
	fi
}

function led_heartbeat() {
	local led=$1
	[ -f $led/trigger ] && echo heartbeat > $led/trigger
}

function green_heartbeat() {
	led_heartbeat /sys/class/leds/led0
}

function red_heartbeat() {
	led_heartbeat /sys/class/leds/led1
}


TARGET_DEV=$1
red_heartbeat
confirm_dev ${TARGET_DEV}

set -e

unmount_partitions $TARGET_DEV
rewrite_partition_table $TARGET_DEV
format_partitions $TARGET_DEV

green_heartbeat
