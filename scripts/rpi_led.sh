#!/bin/sh

select_led() {
  local LED_PREFIX="/sys/class/leds"

  case "$1" in
    "green")
      echo "$LED_PREFIX/led0"
      ;;
    "red")
      echo "$LED_PREFIX/led1"
      ;;
  esac
}

set_led_key_value() {
	local led=$1
	local key=$2
	local val=$3

	echo $val | sudo tee "$led/$key" >/dev/null
}

list_triggers() {
	local color=$1
	local led="$(select_led $color)"

	cat "$led/trigger" | sed -e 's/\[//g' -e 's/\]//g'
}

led_info() {
	local color=$1
	local led="$(select_led $color)"

	cat <<EOF
path:		"$led"
triggers:	$(cat $led/trigger)
brightness:	$(cat $led/brightness)
EOF
}

set_trigger() {
	local color=$1
	local led="$(select_led $color)"
	shift
	local trigger=$1
	shift

	set_led_key_value "${led}" "trigger" "${trigger}"

	if [ $# -ge 1 ]; then
		local brightness=$1
		set_led_key_value "${led}" "brightness" "${brightness}"
	fi
}

if [ $# -lt 1 ]; then
	cat <<-USAGE
	$0 <color> [<trigger> [<brightness>]]

		color:			red | green
		trigger:		$(list_triggers)
		brightness:		[optional, using "none" trigger] 0 (off) | 1 (on)
	USAGE
	exit 1
fi

if [ $# -eq 1 ]; then
	led_info $1
else
	set_trigger $@
fi

