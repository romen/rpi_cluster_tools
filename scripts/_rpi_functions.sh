#!/bin/bash

function rpi_get_serial() {
    sed -n -E 's/^Serial\s*:\s*[0-9a-fA-F]+([0-9a-fA-F]{8})/\1/p' /proc/cpuinfo
}

function rpi_serial_to_mac() {
    if [[ $# -ge 1 ]]; then
        local RPF_OUI="b8:27:eb"
        local LSB=$(echo $1 | sed -n -E 's/.*([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})$/\1:\2:\3/p')

        echo "${RPF_OUI}:${LSB}"
    else
        ip link show eth0|sed -n -E 's_.*link/ether\s([0-9a-fA-F:]+).*_\1_p'
    fi
}

function rpi_mac_to_ip6_linklocal() {
    python3 <<EOS
#!/usr/bin/env python3

import ipaddress
import re

def mac2eui64(mac, prefix=None):
    '''
    Convert a MAC address to a EUI64 address
    or, with prefix provided, a full IPv6 address
    '''
    # http://tools.ietf.org/html/rfc4291#section-2.5.1
    eui64 = re.sub(r'[.:-]', '', mac).lower()
    eui64 = eui64[0:6] + 'fffe' + eui64[6:]
    eui64 = hex(int(eui64[0:2], 16) ^ 2)[2:].zfill(2) + eui64[2:]

    if prefix is None:
        return ':'.join(re.findall(r'.{4}', eui64))
    else:
        try:
            net = ipaddress.ip_network(prefix, strict=False)
            euil = int('0x{0}'.format(eui64), 16)
            return str(net[euil])
        except:  # pylint: disable=bare-except
            return



print(mac2eui64(mac='$(rpi_serial_to_mac $@)', prefix='fe80::/64'))

EOS
}

function sshpi() {
  local serial=$1
  shift
  local pi_ip=$(rpi_mac_to_ip6_linklocal $serial)

  ssh	-o "UserKnownHostsFile /dev/null" \
	-o StrictHostKeyChecking=no \
	-o LogLevel=error \
	-o ConnectTimeout=${SSHPI_TIMEOUT:-5} \
	pi@"${pi_ip}%eth1" $@

}

