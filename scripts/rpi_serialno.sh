#!/bin/bash

THIS_SOURCE="${BASH_SOURCE[0]}"
while [ -h "$THIS_SOURCE" ]; do # resolve $THIS_SOURCE until the file is no longer a symlink
  RPI_SCRIPTS_DIR="$( cd -P "$( dirname "$THIS_SOURCE" )" && pwd )"
  THIS_SOURCE="$(readlink "$THIS_SOURCE")"
  [[ $THIS_SOURCE != /* ]] && THIS_SOURCE="$RPI_SCRIPTS_DIR/$THIS_SOURCE" # if $THIS_SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
export RPI_SCRIPTS_DIR="$( cd -P "$( dirname "$THIS_SOURCE" )" && pwd )"

source "${RPI_SCRIPTS_DIR}/_rpi_functions.sh"

if [[ "$1" == "-6" ]]; then
    shift
    rpi_mac_to_ip6_linklocal $@
    exit
elif [[ "$1" == "-m" ]]; then
    shift
    rpi_serial_to_mac $@
    exit
fi

rpi_get_serial
