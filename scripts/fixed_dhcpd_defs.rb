#!/usr/bin/env ruby

require 'ipaddr'

$INPUT=ARGF
$IP_START="172.22.10.1"


def rpi_serial_to_mac(serial)
	rpf_oui="b8:27:eb"
	serial=serial.downcase
	t = serial.scan(/[a-f0-9]{2}/)[-3,3] # grab only last three bytes

	rpf_oui + ":" + t.join(":")
end

ip = IPAddr.new($IP_START, Socket::AF_INET)
h=$INPUT.readlines.map do |l|
	l=l.strip
	hip=ip
	ip=ip.succ
	{ mac: rpi_serial_to_mac(l), ip: hip , serial: l , hostname: "rpi-"+l}
end

o=h.map do |h|
"host #{h[:hostname]} {
	fixed-address #{h[:ip]};
	hardware ethernet #{h[:mac]};
}"
end

puts o
